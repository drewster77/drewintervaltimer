//
//  DrewTimerInterval.swift
//  by Andrew Benson
//
//  Created by Andrew Benson on 5/28/16.
//  Copyright © 2016 Nuclear Cyborg Corp. All rights reserved.
//

import Foundation

struct DrewIntervalTimer: CustomStringConvertible {
    var begin: CFAbsoluteTime
    var end: CFAbsoluteTime
    var lapCount: Int = 0
    var label: String = ""
    var iterationsMode: Bool = false
    var aggregateTime: Double

    init() {
        begin = CFAbsoluteTimeGetCurrent()
        end = 0
        iterationsMode = false
        aggregateTime = 0.0
        start()
    }
    mutating func start() {
        begin = CFAbsoluteTimeGetCurrent()
        end = 0
        if !iterationsMode {
            lapCount = 0
        }
    }

    @discardableResult
    mutating func stop() -> Double {
        if end == 0 {
            end = CFAbsoluteTimeGetCurrent()
            lapCount += 1
        }

        let interval = Double(end - begin)
        if iterationsMode {
            aggregateTime += interval
        }
        return interval
    }

    @discardableResult
    mutating func stop(_ message: String) -> Double {
        let final = self.stop()
        print("\(message): \(description)")
        return final
    }

    @discardableResult
    mutating func lap() -> Double {
        lapCount += 1

        return Double(CFAbsoluteTimeGetCurrent() - begin)
    }

    var duration: CFAbsoluteTime {
        get {
            if end == 0 {
                return CFAbsoluteTimeGetCurrent() - begin
            } else {
                return end - begin
            }
        }
        set {
            end = newValue
            begin = 0
        }
    }

    var description: String {
        return getFormattedDuration(duration)
    }

    func getFormattedDuration(_ duration: CFAbsoluteTime) -> String {
        let time = duration
        if time > 100 {
            return " \(String(format: "%.1f", time/60)) min"
        } else if time < 1e-6 {
            return " \(String(format: "%.1f", time*1e9)) ns"
        } else if time < 1e-3 {
            return " \(String(format: "%.1f", time*1e6)) µs"
        } else if time < 1 {
            return " \(String(format: "%.1f", time*1000)) ms"
        } else {
            return " \(String(format: "%.2f", time)) s"
        }
    }

    mutating func getSummaryDescription() -> String {
        if iterationsMode {
            let averageTime = aggregateTime / Double(lapCount)

            let s = "\(label):\n  # Samples: \(lapCount)\n  Total time: \(getFormattedDuration(aggregateTime))\n  Average: \(getFormattedDuration(averageTime))\n"
            return(s)
        } else {
            let finishingTime = end != 0 ? end : CFAbsoluteTimeGetCurrent()
            let elapsedTime = finishingTime - begin
            let averageTime = elapsedTime / Double(lapCount)

            let s = "\(label):\n  # Samples: \(lapCount)\n  Total time: \(getFormattedDuration(elapsedTime))\n  Average: \(getFormattedDuration(averageTime))\n"
            return(s)
        }
    }
}
